
	import java.util.Arrays;
	
	public class Block {
	    private int previousHash;
	    private String data;
	    private int hash;
	    public Block(String data, int previousHash) {
	        this.data = data;
	        this.previousHash = previousHash;
	        // Mix the content of this block with previous hash to create the hash of this new block
	        // and that's what makes it block chain
	        this.hash  = Arrays.hashCode(new Integer[]{data.hashCode(), previousHash});
	    }
		
	    //getter and setter
		public String getData() {
			return data;
		}
		public void setData(String data) {
			this.data = data;
		}
		public void setPreviousHash(int previousHash) {
			this.previousHash = previousHash;
		}
		public void setHash(int hash) {
			this.hash = hash;
		}
		
		public int getHash() {
			// TODO Auto-generated method stub
			return 0;
		}
		public int getPreviousHash() {
			// TODO Auto-generated method stub
			return 0;
		}
	}